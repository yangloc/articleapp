SinglePostController = BaseController.extend({
  template: 'singlePost',

  data: function() {
	var articleID = this.params._id;
	var commentsData = Comments.find({articleId: articleID});
	var commentsCount = commentsData.count();
    return {
		post: Posts.findOne(articleID),
		comments: commentsData,
		articleId: articleID,
		commentsCount: commentsCount
	}
  }
});