// Server side

var commentFields = {
  author: {
    type: String,
	label: 'Your ID'
  },
  body: {
    type: String,
    label: 'Comment',
    autoform: {
      type: 'textarea',
      rows: 4
    }
  },
  articleId: {
	type : String,
	optional: true,
	autoform: {
      afFieldInput: {
        type: "hidden"
      }
    }
  },
  _id: {
    type: String,
    optional: true,
    autoform: {
      omit: true
    }
  },
  userId: {
    type: String,
    optional: true,
    autoform: {
      omit: true
    }
  },
  createdAt: {
    type: Date,
    optional: true,
    autoform: {
      omit: true
    }
  }
};

CommentSchema = new SimpleSchema(commentFields);

Meteor.methods({
  submitComment: function(comment) {

    var additionalParams = {
      createdAt: new Date(),
	  // articleId: articleId,
    }

    _.extend(comment, additionalParams);
    comment._id = Comments.insert(comment);
    return comment;
  }
});