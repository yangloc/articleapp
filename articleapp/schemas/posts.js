// Server side

var postFields = {
  title: {
    type: String,
    label: 'Title'
  },
  slug: {
	  type: String,
	  optional: true,
	  autoform: {
      omit: true
    }
  },
  author: {
    type: String
  },
  body: {
    type: String,
    label: 'Body',
    autoform: {
      type: 'textarea',
      rows: 5
    }
  },
  _id: {
    type: String,
    optional: true,
    autoform: {
      omit: true
    }
  },
  userId: {
    type: String,
    optional: true,
    autoform: {
      omit: true
    }
  },
  createdAt: {
    type: Date,
    optional: true,
    autoform: {
      omit: true
    }
  }
};

PostSchema = new SimpleSchema(postFields);

Meteor.methods({
  submitPost: function(post) {	
    var additionalParams = {
      createdAt: new Date(), 
    };

    _.extend(post, additionalParams);
    post._id = Posts.insert(post);

    return post;
  }
});