Posts = new Mongo.Collection('posts');
Posts.attachSchema(PostSchema);
Comments = new Mongo.Collection('comments');
Comments.attachSchema(CommentSchema);

if (Meteor.isClient) {
	
	Template.newPost.events({
		'submit form' : function (event) {
			alert('You just post another article!');
			Router.go('/');
		}
	});
	
	// TODO: refactor templates
	Template.post.helpers({
		timeDiff : function() {
			console.log(this);
			var diff = moment(this.createdAt).from(moment());
			return diff;
		}
	});
	
	Template.singleComment.helpers({
		timeDiff : function() {
			var diff = moment(this.createdAt).from(moment());
			return diff;
		}
	});
	
	Template.displayComments.events({
		'click #showComment' : function(event) {
			$('#showComment').fadeOut('fast');
			$('.commentsList').fadeIn('slow');
		},
		'click #showSubmitComment' : function(event) {
			$('#showSubmitComment').fadeOut('fast');
			// TODO add autoScroll
			$('#submitCommentForm').fadeIn('slow');
		}
	});
	
	Template.newComment.events({
		'submit form' : function (event) {
			alert('You just submitted a comment!');
		},
		
	});
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
