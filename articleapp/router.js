Router.configure({
	loadingTemplate: 'spinner',
	notFoundTemplate: 'notFound'
});

Router.route('/', {
  name: 'root',
  controller: 'MainPageController',
});

Router.route('/newPost', {
  controller: 'BaseController'
});

Router.route('/articles/:_id', {
  name: 'singlePost',
  controller: 'SinglePostController'
});